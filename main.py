import os
import time
from argparse import ArgumentParser

parser = ArgumentParser(description="Python script for delete old log files in a directory")

parser.add_argument('--max_dir_size', type=int, help='Size of a folder to trigger the script. Size in megabytes.', default=10000)
parser.add_argument('dir_path', type=str, help='Absolute path to the log folder')

args = parser.parse_args()

max_dir_size = args.max_dir_size
dir_path = args.dir_path

candidate_to_delete = {}
total_dir_size = 0


def get_file_age_in_days(file_path: str) -> float:
    # Get the time of the last modification of the file
    file_modification_time = os.path.getmtime(file_path)
    # Get the current time
    current_time = time.time()
    # Calculate the difference in seconds
    difference_in_seconds = current_time - file_modification_time
    # Convert the difference to days and return
    return difference_in_seconds


# find max files and get size of dir
for file in os.scandir(dir_path):
    if os.path.isfile(file.path):
        file_size_mb = round(os.path.getsize(file.path) / (1024 ** 2), 1)
        file_age = get_file_age_in_days(file.path)
        total_dir_size += file_size_mb
        print(f"File: {file.name} -- {file_size_mb} MB")
        candidate_to_delete[file.path] = file_age

print(f"\nTotal folder size: {total_dir_size} MB")

# check if total_dir_size exceeds max_dir_size and delete old files if necessary
if total_dir_size >= max_dir_size:
    for path, age in sorted(candidate_to_delete.items(), key=lambda item: item[1], reverse=True):
        total_dir_size -= round(os.path.getsize(path) / (1024 ** 2), 1)
        os.remove(path)
        print(f"Deleted file {path}")
        if total_dir_size < max_dir_size:
            break
    print(f"\nTotal folder size after delete files: {total_dir_size} MB")
    
