# Python Script for Deleting Old Log Files
This Python script is designed to manage the size of a directory by deleting old log files when the total size of the directory exceeds a certain limit.

## How it Works

The script scans through the specified directory and calculates the size of each file and the total size of the directory. If the total size exceeds the specified limit, the script sorts the files by their age and deletes the oldest files until the total size is below the limit.



## Usage
Run the script using Python and provide the required arguments.
```bash
python main.py [--max_dir_size MAX_DIR_SIZE] dir_path
```
## Arguments

`--max_dir_size MAX_DIR_SIZE`: The maximum size for the directory in megabytes. If the total size of the directory exceeds this limit, the script will start deleting old files. This is an `optional` argument. If not provided, the script will use a default value of `10000 MB`.

`dir_path`: The absolute path to the directory you want to manage. This is a required argument.

The script will scan the directory and calculate the total size of the directory. If the total size is greater than or equal to `max_dir_size`, then the script will delete all old files in the folder, while size of the folder will be less then `max_dir_size`.

## Example
```bash
python main.py --max_dir_size 5000 /path/to/logs
```
This command will manage the directory at `/path/to/logs`, deleting old files if the total size exceeds `5000 MB`.

## Output
The script prints out the name and size of each file it processes. It also prints the total size of the directory before and after any deletions. If it deletes any files, it will print out the name of each file it deletes.

Please note that this script only deletes files, not subdirectories. If there are any subdirectories in the specified directory, the script will ignore them.